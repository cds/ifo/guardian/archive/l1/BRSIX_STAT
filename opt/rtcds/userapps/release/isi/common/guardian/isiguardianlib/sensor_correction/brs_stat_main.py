#!/usr/bin/env python
"""BRS.py - Monitor the Beam Rotation Sensor and determine 
which of the 4 states it is in.

States:
DAMPER_ON_HIGH_VEL
DAMPER_ON
READY
FAULT
"""
import time
from guardian import GuardState, GuardStateDecorator
from cdsutils import getdata

########################################
# Constants

# Initial request state, set on initialization
request = 'READY'

drift_threshold = 14000
########################################
# Decorators and functions
class Check_Drift(GuardStateDecorator):
    def pre_exec(self):
        if abs(ezca['DRIFTMON']) > drift_threshold:
            return 'OUT_OF_RANGE'

def Check_BRS():
    """This is the main function used to check on the status of the BRS.
    With the new Beckhoff computers it is now much easier to figure out 
    the state of the BRS :)
    """
    # This one is easy, just need to look at the DAMPCTRLMON 
    if ezca['DAMPCTRLMON'] > 8000:
        return 'READY'
    elif 8000 > ezca['DAMPCTRLMON'] > 3000:
        v_chan = '{}VEL'.format(ezca.prefix)
        counter = 0
        # Try 3 times
        while counter < 4:
            try:
                # FIXME: due to an nds bug, we cant reliably get data in the past
                # when the timeframe spans data that is both on memory and disk.
                # For now, we have to get more data than we want
                #dat = getdata(v_chan, -120)
                dat = getdata(v_chan, 300, start=(ezca[':FEC-102_TIME_DIAG']-305))
                if not dat:
                    counter += 1
                    continue
                else:
                    dat = dat.data[180*16:]
                    break
            except RuntimeError:
                counter += 1
                continue
        if counter == 4:
            # IDK, just go here until you can get data again
            return 'DAMPER_ON'
        elif max(dat.data) - min(dat.data) > 10000:
            notify('BRS velocity is high')
            return 'DAMPER_ON_HIGH_VEL'
        else:
            return 'DAMPER_ON'
    elif abs(ezca['DRIFTMON']) > drift_threshold:
        return 'OUT_OF_RANGE'
    else:
        return 'FAULT'

##################################################
# States
'''
# FIXME: The following doesnt work because of the self.__clas__.__name__
# There should be a way to reference the name of the current state, or
# a different way to do this.

def gen_brs_idle_state(index, requestable=True):
    """State generator that almost all states should use.
    Run the check every 30sec to save the nds a bit, or jump
    to the appropriate state.
    """
    class BRS_IDLE_STATE(GuardState):
        request = requestable
        def main(self):
            self.timer['run check'] = 0
        @Check_Drift
        def run(self): 
            if self.timer['run check']:
                where_BRS_should_be = Check_BRS()
                log(self.__class__.__name__)
                if where_BRS_should_be == self.__class__.__name__:
                    log('I made it here')
                    # Run the check every 30sec to not hammer nds
                    self.timer['run check'] = 30
                    return True
                else:
                    log('not here')
                    return where_BRS_should_be
    BRS_IDLE_STATE.index = index
    return BRS_IDLE_STATE
'''
    
class READY(GuardState):
    index = 40
    def main(self):
        self.timer['run check'] = 0
    @Check_Drift
    def run(self): 
        if self.timer['run check']:
            where_BRS_should_be = Check_BRS()
            if where_BRS_should_be == self.__class__.__name__:
                # Run the check every 30sec to not hammer nds
                self.timer['run check'] = 30
            else:
                return where_BRS_should_be
        return True


class DAMPER_ON(GuardState):
    index = 30
    request = False
    def main(self):
        self.timer['run check'] = 0
    @Check_Drift
    def run(self): 
        if self.timer['run check']:
            where_BRS_should_be = Check_BRS()
            if where_BRS_should_be == self.__class__.__name__:
                # Run the check every 30sec to not hammer nds
                self.timer['run check'] = 30
            else:
                return where_BRS_should_be
        return True


class DAMPER_ON_HIGH_VEL(GuardState):
    index = 20
    request = False
    def main(self):
        self.timer['run check'] = 0
    @Check_Drift
    def run(self): 
        if self.timer['run check']:
            where_BRS_should_be = Check_BRS()
            if where_BRS_should_be == self.__class__.__name__:
                # Run the check every 30sec to not hammer nds
                self.timer['run check'] = 30
            else:
                return where_BRS_should_be
        return True


class FAULT(GuardState):
    index = 10
    request = False
    def run(self): 
        if abs(ezca['DRIFTMON']) > drift_threshold:
            notify('BRS out of range')
            return 'OUT_OF_RANGE'
        if ezca['USER'] == 0:
            notify('Damping is OFF')
            return False
        where_BRS_should_be = Check_BRS()
        if where_BRS_should_be == self.__class__.__name__:
            # Slow the loop down a bit, then move on
            time.sleep(1)
            return True
        else:
            return where_BRS_should_be


class OUT_OF_RANGE(GuardState):
    index = 15
    request = False
    def main(self):
        # 10min seems like a reasonable amount of time. I guess.
        self.timer['in_out_threshold'] = 600
    def run(self):
        if abs(ezca['DRIFTMON']) > drift_threshold:
            notify('BRS out of range')
            # Wait another 5? Sure
            self.timer['in_out_threshold'] = 300
            return False
        elif self.timer['in_out_threshold'] and abs(ezca['DRIFTMON']) <= drift_threshold:
            where_BRS_should_be = Check_BRS()
            return where_BRS_should_be
        else:
            return False

class INIT(GuardState):
    """Run like other idle states but without sleep.

    """
    @Check_Drift
    def main(self):
        where_BRS_should_be = Check_BRS()
        return where_BRS_should_be
    
    
# No edges, it should just be jump states
